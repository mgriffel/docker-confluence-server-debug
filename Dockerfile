FROM atlassian/confluence-server:6.5

LABEL Description="This image is used to start Atlassian Confluence in debug mode" Vendor="mgriffel" Version=6.5-1

MAINTAINER mgriffel <michael.griffel@gmail.com>

ENV RUN_USER            daemon
ENV RUN_GROUP           daemon

ENV JPDA_ADDRESS="5000"
ENV JPDA_TRANSPORT="dt_socket"

CMD ["./bin/catalina.sh", "jpda", "run"]
